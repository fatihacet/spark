# Spark Framework

The Spark framework is a brand new JavaScript framework for modern web development. Focused on creating View based components, it aims to be test driven and well documented.

Spark is heavily inspried from [Koding's KD Framework](https://github.com/koding/kd), and is built on top of Google's powerful Closure Library and awesome Closure Tools.


### Future Plans
- Various components like TabPanel, GridPanel, Loader, Modal etc.
- Hassle free Router management.
- Supporting much better templating solution with Closure Templates and ReactJS.
- Custom build options for getting the minumum required parts of the framework for your own needs.
- Advanced compilation support for all framework.
- Create two versions of the framework which will be used in development and production. Allow production version to be used without Closure Library and Closure Tools which is the ultimate goal of this project. A framework with has a lot of features in it, but just 85Kb or less.


### Disclaimer
Spark is currently under development and not ready for using at production. Tests and/or advanced compilation may fail.

